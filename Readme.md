# Gitleaks for GitLab endpoints

To protect team member secrets and prevent leaks proactively this repository
provides some pre-configured Git hooks.

**Note** running the setup scripts might mess with your local Git setup if you
use custom hooks or a global hooks directory. Especially tools like [`lefthook`](https://github.com/evilmartians/lefthook)
are not compatible with this installer.

## Installation

The easy way:

Execute the following command line in your terminal, this will work on both Linux and Mac machines:

```shell
bash <(curl https://gitlab.com/gitlab-com/gl-security/security-research/gitleaks-endpoint-installer/-/raw/main/install.sh)
```

The manual way:

Clone this repo to `$HOME/.gitlab-gitleaks`, then change into `.gitlab-gitleaks`.
Next run `install_gitleaks.sh` followed by `setup_hook.sh`. That's it, you should
be done.

```shell
git clone https://gitlab.com/gitlab-com/gl-security/security-research/gitleaks-endpoint-installer.git "$HOME/.gitlab-gitleaks"
cd "$HOME/.gitlab-gitleaks"
./install_gitleaks.sh
./setup_hook.sh
```

## Update

Run the following commands in a terminal:

```
cd ~/.gitlab-gitleaks
git pull
./install_gitleaks.sh
```

## Uninstallation

Use `git config -e --global` to edit your global Git configuration and delete
the line defining `hooksPath` under the `[core]` section.  Afterwards you can
delete the installation directory `$HOME/.gitlab-gitleaks`.

## Under the hood

Currently this project is pretty simple, it will deploy a user-wide `core.hooksPath`
setting for Git. This `hooksPath` will point to the [`pre-commit`](./hooks/pre-commit) script.
That script will invoke `gitleaks` on every commit and abort if secrets are found,
pointing the user to [this handbook page](https://about.gitlab.com/handbook/engineering/security/gitleaks.html).
