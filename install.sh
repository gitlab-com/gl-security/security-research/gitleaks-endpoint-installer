#!/bin/bash -e

TARGETDIR=$HOME/.gitlab-gitleaks
LEFTHOOK=$(lefthook version 2> /dev/null || echo -n "not installed" )

if [[ ! "$LEFTHOOK" == "not installed" ]]
then
  echo "Installation aborted!"
  echo
  echo "You seem to have lefthook installed, unfortunately it's not compatible with this gitleaks installation."
  exit -1
fi

if [ ! -d "$TARGETDIR" ]
then
  echo "$TARGETDIR does not exist, cloning gitlab-gitleaks into it."
  git clone https://gitlab.com/gitlab-com/gl-security/security-research/gitleaks-endpoint-installer.git $TARGETDIR
  cd "$TARGETDIR"
  ./install_gitleaks.sh
  HOOK_SET=$(git config --global --get core.hooksPath || echo -n "not set")
  if [[ "$HOOK_SET" == "not set" ]]
  then
    echo "Now a global hooksPath for your git installation will be set. See https://git-scm.com/docs/git-config#Documentation/git-config.txt-corehooksPath for details"
    echo
    echo "This will cause issues if you are using githooks already, so please press any key to continue or CTRL-C to abort."
    read -n 1
    ./setup_hook.sh
  else
    echo "A global hooksPath is already set, aborting!"
    exit -1
  fi
else
  echo "$TARGETDIR exists, trying to update."
  cd "$TARGETDIR"
  git pull
  echo "Trying to update gitleaks"
  ./install_gitleaks.sh
fi

echo "All done."
