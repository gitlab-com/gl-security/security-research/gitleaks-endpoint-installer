#!/bin/bash -e

VERSION=8.5.2
UNAMES="$(uname -s)"
case "${UNAMES}" in
    Linux*)     OS=linux;;
    Darwin*)    OS=darwin;;
esac

UNAMEM="$(uname -m)"
case "${UNAMEM}" in
    arm64*)     ARCH=arm64;;
    x86_64*)    ARCH=x64;;
esac

SHASUM="$(which shasum 2> /dev/null || true)"

curl https://github.com/zricethezav/gitleaks/releases/download/v${VERSION}/gitleaks_${VERSION}_${OS}_${ARCH}.tar.gz -L -O

if [ -z "$SHASUM" ]
then
  echo "shasum not found, skipping integrity check"
else
  shasum -c checksums.txt -a 256 --ignore-missing || ( echo "Checksum verification of gitleaks failed" ; exit -1 )
fi
tar -xzf gitleaks_${VERSION}_${OS}_${ARCH}.tar.gz gitleaks
rm gitleaks_${VERSION}_${OS}_${ARCH}.tar.gz
